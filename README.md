# flask-tester

A Flask based app to inspect various aspects of the infrastructure surrounding the container it runs in.

## Usage

The image is available at `registry.gitlab.com/mkamner/flask-tester:latest`
for direct use with any OCI compatible container runtime.

### Run locally using podman (or docker)

```bash
podman run -it --rm -p 8080:8080 registry.gitlab.com/mkamner/flask-tester:latest

docker run -it --rm -p 8080:8080 registry.gitlab.com/mkamner/flask-tester:latest
```

### Run as pod in k8s

```bash
kubectl run flask-tester --image=registry.gitlab.com/mkamner/flask-tester:latest --port=80 --restart=Never
```

## Functionality

The app provides multiple routes to inspect various aspects of the request and container environment:

- `/` (GET,HEAD,OPTIONS): Responds with requests headers and available routes
- `/env-dump` (GET,HEAD,OPTIONS): Responds with all set environment variables
- `/env-inspect/<string:var_list>` (GET,HEAD,OPTIONS): Responds with the values of all specified environment variables (comma separated list),
  warning if they are not set
- `/headers` (GET,HEAD,OPTIONS): Responds with requests headers 

In addition there are ways to simulate specific responses for troubleshooting:

- `/delay/<int|float:delay>` (GET,HEAD,OPTIONS): Responds with HTTP/200 after the given delay in seconds
- `/status-code/<int:code>` (GET,HEAD,OPTIONS): Responds with the specified status code

## About

This is a tool I developed to quickly troubleshoot issues with workloads in Kubernetes and other container platforms,
if it helps you like it does me consider [buying me a coffee](https://www.buymeacoffee.com/mkamner) or starring the project!