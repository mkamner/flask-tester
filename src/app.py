import html
import operator
import os
import time

from flask import Flask, request


app = Flask(__name__)


def htmlify_headers():
    """HTML-ify request header information"""
    return "<h2>Headers</h2>" + "<br>".join([f"{header}: {value}" for header, value in sorted(request.headers.items())])


def htmlify_routes():
    """HTML-ify the apps route information"""
    rules = []
    for rule in app.url_map.iter_rules():
        methods = ",".join(sorted(rule.methods))
        rules.append((rule.endpoint, methods, str(rule)))

    routes = []
    sort_by_rule = operator.itemgetter(2)
    for endpoint, methods, rule in sorted(rules, key=sort_by_rule):
        route = html.escape(f"{rule} ({methods}): {endpoint}")
        routes.append(route)

    return "<h2>Routes</h2>" + "<br>".join(routes)


def htmlify_env(var_list):
    """HTML-ify given environment variables and their values"""
    return "<h2>Environment Variables</h2>" + "<br>".join(
        [f"{key}: {os.environ.get(key, '!! not set !!')}" for key in sorted(var_list)]
    )


def info_response(h1="flask-tester", code=200):
    """Response"""
    response = "<hr>".join(
        [
            f"<h1>{h1}</h1>",
            f"Response code: {code}",
            htmlify_headers(),
            htmlify_routes(),
            "flask-tester <a href='https://gitlab.com/mkamner/flask-tester'>source code on GitLab</a>",
        ]
    )
    return (response, code)


@app.route("/")
def index():
    return info_response()


@app.route("/headers")
def headers():
    """Dump all request headers"""
    return info_response()


@app.route("/status-code/<int:code>")
def status_code(code):
    """Respond with the given HTTP status code"""
    return info_response(h1="Responding with requested status code", code=code)


@app.route("/delay/<float:delay>")
@app.route("/delay/<int:delay>")
def delayed_response(delay):
    """Delay the response by the given time in seconds"""
    time.sleep(delay)
    return info_response(h1=f"Responding with delay of {delay} seconds")


@app.route("/env-inspect/<string:var_list>")
def env_inspect(var_list):
    """Inspect the application environment by dumping specified comma-separated environment variables"""
    var_list = var_list.split(",")
    return htmlify_env(var_list)


@app.route("/env-dump")
def env_dump():
    """Dump all appliation environment variables"""
    return htmlify_env(os.environ.keys())
